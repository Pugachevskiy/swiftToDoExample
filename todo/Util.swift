//
//  Util.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 13.08.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//

import UIKit

class Util {
    public static func showAlert(view: UIViewController, title: String, message: String, handler: (((UIAlertAction)) -> Void)? ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        view.present(alert, animated: true, completion: nil)
    }
}
