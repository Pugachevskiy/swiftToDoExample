//
//  ViewController.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 11.04.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {
    
    // MARK: - Textfield outlet
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // MARK: - Varibles
    
    var login = ""
    var user = User()
    
    // MARK: - Buttons
    
    @IBAction func signInButton(_ sender: Any) {
        login = loginTextField.text!
        guard let password = passwordTextField.text else {
            return
        }
        
        guard user.userExist(login: login) else {
            Util.showAlert(view: self, title: "Auth failure", message: "Wrong login", handler: nil)
            return
        }
        
        let results = user.getUser(login: login)
        if login == results?.login! && password == results?.password! {
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        } else {
            Util.showAlert(view: self, title:  "Auth failure", message: "Wrong password", handler: nil)
        }
    }
    
    
    @IBAction func signUpButton(_ sender: Any) {
        guard let login = loginTextField.text, login != "" else {
            Util.showAlert(view: self, title: "User name empty", message: "Choose another login", handler: nil)
            return
        }
        guard let password = passwordTextField.text, password != "" else {
            Util.showAlert(view: self, title: "User password empty", message: "Choose another password", handler: nil)
            return
        }
        
        if user.createUser(login: login, password: password) {
            Util.showAlert(view: self, title: "User created", message: "New user successful created", handler: nil)
        } else {
            Util.showAlert(view: self, title: "User exist", message: "Choose another login", handler: nil)
        }
    }
    
    @IBAction func unwindToLogin(segue:UIStoryboardSegue) {
        loginTextField.text! = ""
        passwordTextField.text! = ""
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? UINavigationController {
            let sVC = destinationVC.topViewController as! MainTableViewController
            sVC.user = login
        }
    }
}

