//
//  MainTableViewController.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 15.04.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//

import UIKit

class MainTableViewController : UIViewController {
    
    
    // MARK: - Outlets

    @IBOutlet var tableViewProject: UITableView!
    
    // MARK: - Variables

    var user : String?
    var arrayProjects = [Project()]
    var project = Project()
    var selectedProject : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTable()
        addRefreshControl()
    }
    
    @IBAction func addProject(_ sender: Any) {
        self.selectedProject = nil
        self.performSegue(withIdentifier: "addProjectSegue", sender: self)
    }
    
    @IBAction func unwindToMain(segue:UIStoryboardSegue) {
        updateTable()
    }
    
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        user = nil
        performSegue(withIdentifier: "unwindToMainWithSegue", sender: self)
    }
    
    func addRefreshControl(){
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tableViewProject.refreshControl = refreshControl
    }
    
    @objc func refreshAction(refreshControl: UIRefreshControl) {
        updateTable()
        refreshControl.endRefreshing()
    }
    
    func updateTable(){
        if let arrayProjects = project.getProjects(user: user!) {
            self.arrayProjects = arrayProjects
            tableViewProject.reloadData()
        }
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AddProjectViewController {
            if segue.identifier == "addProjectSegue"{
                destinationVC.isEditingProject = false
            }
            if segue.identifier == "editProjectSegue" {
                destinationVC.isEditingProject = true
            }
            if let index = self.selectedProject {
                destinationVC.project = self.arrayProjects[index]
            }
            let user = User()
            if let users = user.getUsers() {
                destinationVC.users = users
            }
        }
    }
}

// MARK: - Table view data source

extension MainTableViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayProjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellProject", for: indexPath)
        cell.textLabel?.text = arrayProjects[indexPath.row].name
        cell.detailTextLabel?.text = arrayProjects[indexPath.row].descriptionProject
        return cell
    }
}

// MARK: - Table view delegate

extension MainTableViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            self.selectedProject = indexPath.row
            self.performSegue(withIdentifier: "editProjectSegue", sender: tableView.cellForRow(at: indexPath))
        }
        editAction.backgroundColor = .orange
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
            self.project.deleteProject(name: self.arrayProjects[indexPath.row])
            self.arrayProjects.remove(at: indexPath.row)
            self.tableViewProject.deleteRows(at: [indexPath], with: .automatic)
        }
        deleteAction.backgroundColor = .red
        
        return [editAction,deleteAction]
    }
}
