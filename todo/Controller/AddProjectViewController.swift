//
//  AddProjectViewController.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 29.07.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//

import UIKit

class AddProjectViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var projectName: UITextField!
    @IBOutlet weak var projectDescription: UITextField!
    @IBOutlet weak var responsiblePickerView: UIPickerView!
    
    // MARK: - Variables
    
    var users = [String]()
    var project : Project!
    var isEditingProject: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        responsiblePickerView.dataSource = self
        responsiblePickerView.delegate = self

        if project == nil {
            project = Project()
            project.name = ""
            project.descriptionProject = ""
        } else {
            projectName.text = project.name
            projectDescription.text = project.descriptionProject
            if let row = users.index(of: project.responsible!) {
                responsiblePickerView.selectRow(row, inComponent: 0, animated: false)
            }
        }
    }
    
    // MARK: - Button's function
    
    @IBAction func addProject(_ sender: Any) {
        guard let name = projectName.text, name != "" else  {
            Util.showAlert(view: self, title: "Project name empty", message: "Choose another project name", handler: nil)
            return
        }
        guard let description = projectDescription.text, description != "" else {
            Util.showAlert(view: self, title: "Project description empty", message: "Choose another project description", handler: nil)
            return
        }
        guard let responsibleValue = pickerView(responsiblePickerView, titleForRow: responsiblePickerView.selectedRow(inComponent: 0), forComponent: 0), responsibleValue != "" else {
            return
        }
        
        if isEditingProject {
            if self.project.updeteProject(name: name, description: description)!{
                let message = "Project to user: " + pickerView(responsiblePickerView,
                                                               titleForRow: responsiblePickerView.selectedRow(inComponent: 0),
                                                               forComponent: 0)!
                
                Util.showAlert(view: self, title: "Project updated",
                               message: message,
                               handler: { _ in
                                self.performSegue(withIdentifier: "unwindToVC1WithSegue", sender: self)})
            } else {
                Util.showAlert(view: self, title: "Project wasn't updated", message: "Failure", handler: nil)
            }
        } else {
            if self.project.createProject(name: name, description: description, responsible: responsibleValue, number: 0){
                let message = "Project to user: " + pickerView(responsiblePickerView,
                                                               titleForRow: responsiblePickerView.selectedRow(inComponent: 0),
                                                               forComponent: 0)!
                Util.showAlert(view: self, title: "Project created",
                               message: message,
                               handler: { _ in
                                self.performSegue(withIdentifier: "unwindToVC1WithSegue", sender: self)
                })
            } else {
                Util.showAlert(view: self, title: "Project name existed", message: "Choose another project name", handler: nil)
            }
        }
    }
}

 // MARK: - UIPickerView data source

extension AddProjectViewController : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return users.count
    }
}

 // MARK: - UIPickerView delegate

extension AddProjectViewController : UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return users[row]
    }
}
