//
//  User+CoreDataProperties.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 12.04.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var login: String?
    @NSManaged public var password: String?

}
