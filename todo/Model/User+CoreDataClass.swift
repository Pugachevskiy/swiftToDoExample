//
//  User+CoreDataClass.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 12.04.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//
//

import CoreData


public class User: NSManagedObject {
    convenience init() {
        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.instance.managedObjectContext)
        self.init(entity: entity!, insertInto: CoreDataManager.instance.managedObjectContext)
    }
    
    @nonobjc private class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }
    
    // MARK: - Functions
    
    func getUsers() -> [String]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        do {
            let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) as! [User]
            var userArray = [String]() 
            for user in results {
                if let user = user.login{
                    userArray.append(user)
                }
            }
            return userArray
        } catch {
            print(error)
        }
        
        return nil
    }
    
    func getUser(login: String) -> User? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let predicate = NSPredicate(format: "login == %@", login)
        fetchRequest.predicate = predicate
        
        do {
            let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) as! [User]
            if let user = results.first(where: {$0.login == login}) {
                return user
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    func userExist(login: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let predicate = NSPredicate(format: "login == %@", login)
        fetchRequest.predicate = predicate
        do {
            let count = try CoreDataManager.instance.managedObjectContext.count(for: fetchRequest)
            return count > 0
        } catch {
            print(error)
        }
        return false
    }
    
    func createUser(login: String, password: String) -> Bool{
        if userExist(login: login) {
            return false
        } else {
            let managedObject = User()
            managedObject.login = login
            managedObject.password = password
            CoreDataManager.instance.saveContext()
            return true
        }
    }
}
