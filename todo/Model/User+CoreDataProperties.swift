//
//  User+CoreDataProperties.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 08.06.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//
//

import CoreData


extension User {
    @NSManaged public var login: String?
    @NSManaged public var password: String?
}
