//
//  Project+CoreDataClass.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 08.06.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//
//


import CoreData

public class Project: NSManagedObject {
    convenience init() {
        let entity = NSEntityDescription.entity(forEntityName: "Project", in: CoreDataManager.instance.managedObjectContext)
        self.init(entity: entity!, insertInto: CoreDataManager.instance.managedObjectContext)
    }
    
    @nonobjc private func fetchRequest() -> NSFetchRequest<Project> {
        return NSFetchRequest<Project>(entityName: "Project")
    }
    
    // MARK: - Functions
    
   /* func getProject(name: String) -> Project? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
        let predicate = NSPredicate(format: "name == %@", name)
        fetchRequest.predicate = predicate
        
        do {
            let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) as! [Project]
            if let project = results.first(where: {$0.name == name}) {
                return project
            }
        } catch {
            print(error)
        }
        return nil
    }*/
    
    func getProjects(user: String) -> [Project]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
        let predicate = NSPredicate(format: "responsible == %@", user)
        fetchRequest.predicate = predicate
        do {
            let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) as! [Project]
            return results
        } catch {
            print(error)
        }
        return nil
    }
    
    func createProject(name: String, description: String, responsible:String, number: Int16) -> Bool{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
        let predicate = NSPredicate(format: "name == %@", name)
        fetchRequest.predicate = predicate
        do {
            let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) as! [Project]
            if results.first(where: {$0.name == name}) != nil {
                return false
            } else {
                let managedObject = Project()
                managedObject.name = name
                managedObject.descriptionProject = description
                managedObject.responsible = responsible
                managedObject.numberOfTasks = number
                CoreDataManager.instance.saveContext()
                return true
            }
        } catch {
            print(error)
        }
        return false
    }
    
    func deleteProject(name: Project) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
        if let result = try? CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) {
            for object in result {
                let project = object as! Project
                if project === name {
                    CoreDataManager.instance.managedObjectContext.delete(object as! NSManagedObject)
                }
            }
            CoreDataManager.instance.saveContext()
        }
    }
    
    func updeteProject(name: String, description: String) -> Bool? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Project")
        let predicate = NSPredicate(format: "name == %@", name)
        fetchRequest.predicate = predicate
        do {
            let results = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) as! [Project]
            if results.first(where: {$0.name == name}) != nil {
                // In my case, I only updated the first item in results
                results.first?.descriptionProject = description
                CoreDataManager.instance.saveContext()
                return true
            }
        } catch {
            print("Fetch Failed: \(error)")
            return false
        }
        return false
    }
}
