//
//  Project+CoreDataProperties.swift
//  todo
//
//  Created by Sergey Pugachevskiy on 08.06.18.
//  Copyright © 2018 Sergey Pugachevskiy. All rights reserved.
//
//

import CoreData


extension Project {
    @NSManaged public var name: String?
    @NSManaged public var descriptionProject: String?
    @NSManaged public var numberOfTasks: Int16
    @NSManaged public var responsible: String?
}
